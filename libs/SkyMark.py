#! /usr/bin/env python

import numpy as np
from astropy.coordinates import SkyCoord
from astropy.coordinates import FK5
from .SkyObject import SkyObject
from .suppl_funcs import get_decimal_year


class SkyMark(SkyObject):
    """
    A class for plotting a mark on the sky that represent the current
    telescope location or user specified point.
    """
    def __init__(self, location, mark_type):
        super().__init__(location)
        self.equatorial_coords = SkyCoord(ra=0.0, dec=-90, frame=FK5(equinox="J2000"), unit="deg")
        if mark_type == "click":
            self.color = "m"
            self.marker = "x"
        elif mark_type == "telescope":
            self.color = "blue"
            self.marker = "+"
        self.current_ra = 0
        self.current_dec = -90

    def plot(self, ax):
        self.remove()
        zenith_distance = np.pi/2 - np.radians(self.horizonthal_coords.alt.deg)
        if zenith_distance > np.pi / 2:
            # Marker is below the horizon
            return
        azimuth = np.radians(self.horizonthal_coords.az.deg) + np.pi
        x = zenith_distance * np.sin(azimuth)
        y = - zenith_distance * np.cos(azimuth)
        plt_inst = ax.plot(x, y, marker="o", markersize=15, markeredgecolor=self.color,
                           markerfacecolor="none")[0]
        self.plot_instances.append(plt_inst)
        plt_inst = ax.plot(x, y, marker=self.marker, markersize=15, markeredgecolor=self.color)[0]
        self.plot_instances.append(plt_inst)

    def set_coordinates(self, name, new_ra, new_dec):
        self.name = name
        self.current_ra = new_ra
        self.current_dec = new_dec
        self.equatorial_coords = SkyCoord(ra=new_ra, dec=new_dec, unit="deg",
                                          frame=FK5(equinox="J%1.2f" % get_decimal_year()))

    def get_params(self):
        ra_h, ra_m, ra_s = self.equatorial_coords.ra.hms
        dec_d, dec_m, dec_s = self.equatorial_coords.dec.dms
        alt_d, alt_m, alt_s = self.horizonthal_coords.alt.dms
        az_d, az_m, az_s = self.horizonthal_coords.az.dms
        zenith_distance = 90 - self.horizonthal_coords.alt.deg
        airmass = 1 / np.cos(np.radians(zenith_distance))
        obj_params = {"name": self.name, "ra_h": int(ra_h), "ra_m": int(ra_m), "ra_s": int(ra_s),
                      "dec_d": int(dec_d), "dec_m": int(dec_m), "dec_s": int(dec_s),
                      "alt_d": int(alt_d), "alt_m": int(alt_m), "alt_s": int(alt_s),
                      "az_d": int(az_d), "az_m": int(az_m), "az_s": int(az_s), "airmass": airmass}
        return obj_params
